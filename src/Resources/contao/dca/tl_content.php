<?php

foreach ($GLOBALS['TL_DCA']['tl_content']['palettes'] as $key => $palette) {
    if (!is_array($palette) && is_string($palette)) {
        if (strpos($palette, ",headline")) {
            $GLOBALS['TL_DCA']['tl_content']['palettes'][$key] = str_replace('headline', 'headline,headlineClass', $GLOBALS['TL_DCA']['tl_content']['palettes'][$key]);
        }
    }
}

use Contao\CoreBundle\DataContainer\PaletteManipulator;

$GLOBALS['TL_DCA']['tl_content']['fields']['headlineClass'] = [
    'inputType' => 'text',
    'eval' => array('tl_class'=>'w50'),
    'sql'  => "varchar(255) NOT NULL default ''"
];
