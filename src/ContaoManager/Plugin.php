<?php

namespace designerei\ContaoHeadlineExtendedBundle\ContaoManager;

use designerei\ContaoHeadlineExtendedBundle\ContaoHeadlineExtendedBundle;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Contao\CoreBundle\ContaoCoreBundle;

class Plugin implements BundlePluginInterface
{
    public function getBundles(ParserInterface $parser): array
    {
        return [
            BundleConfig::create(ContaoHeadlineExtendedBundle::class)
                ->setLoadAfter([ContaoCoreBundle::class]),
        ];
    }
}
